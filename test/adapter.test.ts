import { expect } from "chai";
import { ethers } from "hardhat";

const zeroAddress = "0x0000000000000000000000000000000000000000";

describe("DEXAdapter", function () {
  let owner: any, contract: any, acdm: any, tst: any, pop: any, weth: any;

  before(async () => {
    const AdapterFactory = await ethers.getContractFactory("DEXAdapter");
    contract = await AdapterFactory.deploy();
    await contract.deployed();

    [owner] = await ethers.getSigners();
  });

  it("Creates tokens", async () => {
    await expect(contract.initializePairs()).to.be.revertedWith(
      "not initialized"
    );
    expect(await contract.POP()).to.eq(zeroAddress);
    expect(await contract.ACDM()).to.eq(zeroAddress);
    expect(await contract.TST()).to.eq(zeroAddress);
    await contract.initialize();
    expect(await contract.POP()).to.not.eq(zeroAddress);
    expect(await contract.ACDM()).to.not.eq(zeroAddress);
    expect(await contract.TST()).to.not.eq(zeroAddress);
    await expect(contract.initialize()).to.be.revertedWith(
      "initialized already"
    );
    acdm = await contract.ACDM();
    tst = await contract.TST();
    pop = await contract.POP();
    weth = await contract.WETH();
  });

  it("Creates pairs", async () => {
    expect(await contract.getPair(acdm, tst)).to.eq(zeroAddress);
    expect(await contract.getPair(pop, acdm)).to.eq(zeroAddress);
    expect(await contract.getPair(weth, pop)).to.eq(zeroAddress);
    contract.initializePairs();
    expect(await contract.getPair(acdm, tst)).to.not.eq(zeroAddress);
    expect(await contract.getPair(pop, acdm)).to.not.eq(zeroAddress);
    expect(await contract.getPair(weth, pop)).to.not.eq(zeroAddress);
  });

  it("Creates liquidity pools", async () => {
    const acdmContract = await ethers.getContractAt("IERC20", acdm);
    const popContract = await ethers.getContractAt("IERC20", pop);
    const tstContract = await ethers.getContractAt("IERC20", tst);
    const acdmBalance = await acdmContract.balanceOf(contract.address);
    const popBalance = await popContract.balanceOf(contract.address);
    const tstBalance = await tstContract.balanceOf(contract.address);
    contract.initializeLiquidity({ value: ethers.utils.parseEther("10") });
    expect(await acdmContract.balanceOf(contract.address)).to.eq(
      acdmBalance.sub(ethers.utils.parseEther("200"))
    );
    expect(await popContract.balanceOf(contract.address)).to.eq(
      popBalance.sub(ethers.utils.parseEther("10100"))
    );
    expect(await tstContract.balanceOf(contract.address)).to.eq(
      tstBalance.sub(ethers.utils.parseEther("1000"))
    );
  });

  it("Swaps pairs", async () => {
    let acdmPrice = await contract.getPrice(
      ethers.utils.parseEther("10"),
      acdm,
      tst
    );
    console.log(`Price of 10 ACDM: ${ethers.utils.formatEther(acdmPrice)} TST`);
    console.log("Swapping ACDM/TST...");
    const tx = await contract.swap(
      acdm,
      tst,
      ethers.utils.parseEther("10"),
      acdmPrice
    );
    await tx.wait();
    acdmPrice = await contract.getPrice(
      ethers.utils.parseEther("10"),
      acdm,
      tst
    );
    console.log(
      `Price of 10 ACDM after swap: ${ethers.utils.formatEther(acdmPrice)} TST`
    );
  });

  it("Swaps pairs via path", async () => {
    let tstPrice = await contract.getPriceViaPath(
      ethers.utils.parseEther("10"),
      [tst, acdm, pop]
    );
    console.log(`Price of 10 TST: ${ethers.utils.formatEther(tstPrice)} POP`);
    console.log("Swapping TST/POP...");
    const tx = await contract.swapViaPath(
      ethers.utils.parseEther("10"),
      tstPrice,
      [tst, acdm, pop]
    );
    await tx.wait();
    tstPrice = await contract.getPriceViaPath(ethers.utils.parseEther("10"), [
      tst,
      acdm,
      pop,
    ]);
    console.log(
      `Price of 10 TST after swap: ${ethers.utils.formatEther(tstPrice)} POP`
    );
  });

  it("Removes liquidity", async () => {
    const lpContract = await ethers.getContractAt(
      "IERC20",
      await contract.getPair(acdm, tst)
    );
    const acdmContract = await ethers.getContractAt("IERC20", acdm);
    const tstContract = await ethers.getContractAt("IERC20", tst);
    const lpBalance = await lpContract.balanceOf(contract.address);
    console.log("LP Balance:", lpBalance);
    const acdmBalance = await acdmContract.balanceOf(contract.address);
    const tstBalance = await tstContract.balanceOf(contract.address);
    await contract.removeLiquidity(acdm, tst, lpBalance);
    expect(await acdmContract.balanceOf(contract.address)).to.be.gt(
      acdmBalance
    );
    expect(await tstContract.balanceOf(contract.address)).to.be.gt(tstBalance);
  });

  it("Withdraw tokens and lp tokens", async () => {
    const acdmContract = await ethers.getContractAt("IERC20", acdm);
    const acdmBalance = await acdmContract.balanceOf(contract.address);
    await expect(() => contract.withdraw(acdm)).to.changeTokenBalance(
      acdmContract,
      owner,
      acdmBalance
    );
    const lpContract = await ethers.getContractAt(
      "IERC20",
      await contract.getPair(acdm, pop)
    );
    const lpBalance = await lpContract.balanceOf(contract.address);
    await expect(() => contract.withdrawLP(acdm, pop)).to.changeTokenBalance(
      lpContract,
      owner,
      lpBalance
    );
  });

  it("should not get price of unexisting pair", async () => {
    await expect(
      contract.getPrice(1, contract.address, owner.address)
    ).to.be.revertedWith("No such pair");
  });
});

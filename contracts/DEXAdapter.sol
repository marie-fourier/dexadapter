// SPDX-License-Identifier: Unlicensed
pragma solidity 0.8.11;

import "@uniswap/v2-periphery/contracts/interfaces/IUniswapV2Router02.sol";
import "@uniswap/v2-core/contracts/interfaces/IUniswapV2Factory.sol";
import "@uniswap/v2-core/contracts/interfaces/IUniswapV2Pair.sol";
import "@uniswap/v2-periphery/contracts/interfaces/IWETH.sol";
import "@openzeppelin/contracts/access/Ownable.sol";
import "./Token.sol";

contract DEXAdapter is Ownable {
  address private constant FACTORY = 0x5C69bEe701ef814a2B6a3EDD4B1652CB9cc5aA6f;
  address private constant ROUTER = 0x7a250d5630B4cF539739dF2C5dAcb4c659F2488D;
  uint256 private constant MAX_INT = 2**256 - 1;
  Token public POP;
  Token public ACDM;
  Token public TST;

  constructor() {}

  /**
    creates erc20 tokens
  */
  function initialize() external onlyOwner {
    require(
      address(POP) == address(0) &&
      address(ACDM) == address(0) &&
      address(TST) == address(0),
      "initialized already"
    );
    POP = new Token("POP Token", "POP");
    POP.mint(address(this), 10_000_000 ether);
    POP.approve(ROUTER, MAX_INT);
    ACDM = new Token("ACDM Token", "ACDM");
    ACDM.mint(address(this), 10_000_000 ether);
    ACDM.approve(ROUTER, MAX_INT);
    TST = new Token("TST Token", "TST");
    TST.mint(address(this), 10_000_000 ether);
    TST.approve(ROUTER, MAX_INT);
    Token(WETH()).approve(ROUTER, MAX_INT);
  }

  /**
    creates pools for erc20 tokens
   */
  function initializePairs() external onlyOwner {
    require(
      address(POP) != address(0) &&
      address(ACDM) != address(0) &&
      address(TST) != address(0),
      "not initialized"
    );
    IUniswapV2Factory(FACTORY).createPair(address(ACDM), address(TST));
    IUniswapV2Factory(FACTORY).createPair(address(POP), address(ACDM));
    IUniswapV2Factory(FACTORY).createPair(
      WETH(),
      address(POP)
    );
  }

  function initializeLiquidity() external payable onlyOwner {
    addLiquidity(
      address(ACDM),
      address(TST),
      100 ether,
      1000 ether
    );
    addLiquidity(
      address(POP),
      address(ACDM),
      10000 ether,
      100 ether
    );
    IWETH(WETH()).deposit{value: msg.value}();
    addLiquidity(
      address(POP),
      WETH(),
      100 ether,
      msg.value
    );
  }

  function swap(
    address tokenA,
    address tokenB,
    uint256 amountA,
    uint256 amountBMin
  ) public onlyOwner {
    address[] memory path = new address[](2);
    path[0] = tokenA;
    path[1] = tokenB;
    IUniswapV2Router02(ROUTER).swapExactTokensForTokens(
      amountA,
      amountBMin,
      path,
      address(this),
      block.timestamp
    );
  }

  function swapViaPath(
    uint256 amountA,
    uint256 amountBMin,
    address[] memory path
  ) public onlyOwner {
    IUniswapV2Router02(ROUTER).swapExactTokensForTokens(
      amountA,
      amountBMin,
      path,
      address(this),
      block.timestamp
    );
  }

  /**
    @param tokenA - address of a token
    @param tokenB - address of a token
    @param amountADesired - amount of tokenA to add to liquidity
    @param amountB - amount of tokenB to add to liquidity

    Requirements:
      amountB should be greater than amountA * (price of tokenB in terms of tokenA)
  */
  function addLiquidity(
    address tokenA,
    address tokenB,
    uint256 amountADesired,
    uint256 amountBDesired
  ) public returns (uint amountA, uint amountB) {
    (amountA, amountB,) = IUniswapV2Router02(ROUTER).addLiquidity(
      tokenA,
      tokenB,
      amountADesired,
      amountBDesired,
      1,
      1,
      address(this),
      block.timestamp
    );
    (amountA, amountB) = tokenA < tokenB ? (amountA, amountB) : (amountB, amountA);
  }

  function removeLiquidity(
    address tokenA,
    address tokenB,
    uint256 lpTokens
  ) public onlyOwner {
    address pair = IUniswapV2Factory(FACTORY).getPair(tokenA, tokenB);
    Token(pair).approve(ROUTER, MAX_INT);
    IUniswapV2Router02(ROUTER).removeLiquidity(
      tokenA,
      tokenB,
      lpTokens,
      1,
      1,
      address(this),
      block.timestamp
    );
  }

  /**
    Withdraw all lp tokens of a pair tokenA/tokenB
   */
  function withdrawLP(address tokenA, address tokenB) external onlyOwner {
    address pair = IUniswapV2Factory(FACTORY).getPair(tokenA, tokenB);
    assert(
      IUniswapV2Pair(pair).transfer(
        msg.sender,
        IUniswapV2Pair(pair).balanceOf(address(this))
      )
    );
  }

  function withdraw(address token) external onlyOwner {
    uint256 balance = Token(token).balanceOf(address(this));
    Token(token).transfer(msg.sender, balance);
  }

  /**
    @param tokenA - address of a token
    @param tokenB - address of a token

    returns the address of a uniswap pair
   */
  function getPair(address tokenA, address tokenB)
    public
    view
    returns (address pair)
  {
    pair = IUniswapV2Factory(FACTORY).getPair(tokenA, tokenB);
  }

  /**
    @param amount - amount of token b
    @param tokenA - address of a token
    @param tokenB - address of a token
    @return price of tokenA in terms of tokenB

    ############################
    #### VERY BAD METHOD!!! ####
    v2 oracles should be used instead (somewhen in the future)
    ############################
  */
  function getPrice(uint256 amount, address tokenA, address tokenB) public view returns (uint256) {
    address pair = IUniswapV2Factory(FACTORY).getPair(tokenA, tokenB);
    require(pair != address(0), "No such pair");
    (uint112 reserve0, uint112 reserve1,) = IUniswapV2Pair(pair).getReserves();
    return IUniswapV2Router02(ROUTER).getAmountOut(amount, reserve0, reserve1);
  }

  function getPriceViaPath(uint256 amount, address[] memory path) public view returns (uint256) {
    uint256[] memory amounts = IUniswapV2Router02(ROUTER).getAmountsOut(amount, path);
    return amounts[amounts.length - 1];
  }

  function WETH() public pure returns (address) {
    return IUniswapV2Router02(ROUTER).WETH();
  }
}
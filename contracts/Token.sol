// SPDX-License-Identifier: Unlicensed
pragma solidity ^0.8.11;

import "@openzeppelin/contracts/token/ERC20/ERC20.sol";
import "@openzeppelin/contracts/access/Ownable.sol";

contract Token is ERC20, Ownable {
  constructor(
    string memory name,
    string memory symbol
  ) ERC20(name, symbol) {}

  function mint(address addr, uint256 amount) external onlyOwner {
    _mint(addr, amount);
  }

  function burn(address addr, uint256 amount) external onlyOwner {
    _burn(addr, amount);
  }
}
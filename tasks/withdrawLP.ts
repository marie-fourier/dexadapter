import { task } from "hardhat/config";
import { getContract } from "../helpers";

task("withdrawLP")
  .addParam("token0", "Token address")
  .addParam("token1", "Token address")
  .setAction(async (taskArgs: any, hre) => {
    const contract = await getContract(hre);
    contract.withdraw(taskArgs.token0, taskArgs.token1);
  });

import { task } from "hardhat/config";
import { getContract } from "../helpers";

task("pairs").setAction(async (taskArgs: any, hre) => {
  const contract = await getContract(hre);
  const pop = await contract.POP();
  const acdm = await contract.ACDM();
  const tst = await contract.TST();
  const weth = await contract.WETH();
  console.log(`ACDM/TST: ${await contract.getPair(acdm, tst)}`);
  console.log(`POP/ACDM: ${await contract.getPair(pop, acdm)}`);
  console.log(`WETH/POP: ${await contract.getPair(weth, pop)}`);
});

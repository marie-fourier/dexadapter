// We require the Hardhat Runtime Environment explicitly here. This is optional
// but useful for running the script in a standalone fashion through `node <script>`.
//
// When running the script with `npx hardhat run <script>` you'll find the Hardhat
// Runtime Environment's members available in the global scope.
import { ethers } from "hardhat";

async function main() {
  // Hardhat always runs the compile task when running scripts with its command
  // line interface.
  //
  // If this script is run directly using `node` you may want to call compile
  // manually to make sure everything is compiled
  // await hre.run('compile');

  // We get the contract to deploy
  const Adapter = await ethers.getContractFactory("DEXAdapter");
  const adapter = await Adapter.deploy();

  await adapter.deployed();

  console.log("adapter deployed to:", adapter.address);

  let tx = await adapter.initialize({ gasLimit: 3000000 });
  await tx.wait();
  console.log("Created erc20 tokens");
  tx = await adapter.initializePairs({ gasLimit: 10000000 });
  await tx.wait();
  console.log("Created pairs");
  tx = await adapter.initializeLiquidity({
    gasLimit: 10000000,
    value: ethers.utils.parseEther("0.01"),
  });
  await tx.wait();
  console.log("Created liquidity pools");

  const pop = await adapter.POP();
  const acdm = await adapter.ACDM();
  const tst = await adapter.TST();
  const weth = await adapter.WETH();
  console.log(`POP: ${await adapter.POP()}`);
  console.log(`ACDM: ${await adapter.ACDM()}`);
  console.log(`TST: ${await adapter.TST()}`);
  console.log(`ACDM/TST: ${await adapter.callStatic.getPair(acdm, tst)}`);
  console.log(`POP/ACDM: ${await adapter.callStatic.getPair(pop, acdm)}`);
  console.log(`WETH/POP: ${await adapter.callStatic.getPair(weth, pop)}`);

  // ACDM/TST Test
  const acdmPrice = await adapter.callStatic.getPrice(
    ethers.utils.parseEther("10"),
    acdm,
    tst
  );
  console.log(`Price of 10 ACDM: ${ethers.utils.formatEther(acdmPrice)} TST`);

  // TST/POP test
  const tstPrice = await adapter.callStatic.getPriceViaPath(
    ethers.utils.parseEther("10"),
    [tst, acdm, pop]
  );
  console.log(`Price of 10 TST: ${ethers.utils.formatEther(tstPrice)} POP`);
}

// We recommend this pattern to be able to use async/await everywhere
// and properly handle errors.
main().catch((error) => {
  console.error(error);
  process.exitCode = 1;
});

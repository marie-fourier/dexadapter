require("dotenv").config();

export const getContract = async (hre: any) => {
  return await hre.ethers.getContractAt(
    "DEXAdapter",
    String(process.env.CONTRACT_ADDRESS)
  );
};

export const getAccounts = async (hre: any) => {
  return await hre.ethers.getSigners();
};

export const getTimestamp = async (hre: any) => {
  const blockNumber = await hre.ethers.provider.getBlockNumber();
  const block = await hre.ethers.provider.getBlock(blockNumber);
  return block.timestamp;
};

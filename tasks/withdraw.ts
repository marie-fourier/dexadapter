import { task } from "hardhat/config";
import { getContract } from "../helpers";

task("withdraw")
  .addParam("token", "Token address")
  .setAction(async (taskArgs: any, hre) => {
    const contract = await getContract(hre);
    contract.withdraw(taskArgs.token);
  });

import { task } from "hardhat/config";
import { getContract } from "../helpers";

task("tokens").setAction(async (taskArgs: any, hre) => {
  const contract = await getContract(hre);
  console.log(`POP: ${await contract.POP()}`);
  console.log(`ACDM: ${await contract.ACDM()}`);
  console.log(`TST: ${await contract.TST()}`);
});
